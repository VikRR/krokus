$(function () {
    hideRemoveIcon();
    $('#form-wrapper').on('click', '.remove-icon', event => {
        event.target.parentElement.remove();
        hideRemoveIcon();
    });
    $('#add-new-attribute').on('click', addNewAttribute);

});

function addNewAttribute() {
    const formRow = document.getElementsByClassName('form-row'),
        nextIndex = formRow.length,
        cloneFormRow = formRow[nextIndex - 1].cloneNode(true),
        formWrapper = document.getElementById('form-wrapper'),
        inputList = cloneFormRow.querySelectorAll('.form-control');

    inputList.forEach(node => {
        const regexp = new RegExp(`${(nextIndex - 1)}`, "i"),
            newName = node.getAttribute('name').replace(regexp, `${nextIndex}`);
        node.setAttribute('name', newName)
    });
    formWrapper.appendChild(cloneFormRow);
    hideRemoveIcon();
}

function hideRemoveIcon() {
    const removeIcons = document.querySelectorAll('.remove-icon'),
        iconsLength = removeIcons.length;

    if (iconsLength === 1) {
        $(removeIcons[0]).hide();
    } else {
        removeIcons.forEach(node => {
            $(node).show();
        })
    }
}
