<li class="nav-item {{\Request::is($route)?'active': ''}}">
    <a class="nav-link" href="{{ route($routeName) }}">{{ __($item) }}</a>
</li>