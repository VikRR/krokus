@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-8 offset-2">
            <div class="d-flex justify-content-between">
                <h2>Products List</h2>
                <a href="{{route('products.create')}}">
                    <i class="far fa-plus-square fa-lg text-success"></i>
                </a>
            </div>
            @empty(!count($products))
                @foreach($products as $product)
                    <div class="card mb-1">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h4>{{$product->name}}</h4>
                                <a style="display: block" href="{{route('products.edit', ['product'=>$product->id])}}">
                                    <i class="fas fa-pen fa-lg"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <h3>Products Not Found</h3>
            @endempty
        </div>
    </div>
@endsection