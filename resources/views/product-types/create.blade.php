@extends('layouts.app')

@section('content')
    <div class="product-types">
        <div class="row justify-content-center">
            <div class="col-md-8 offset-2">
                @if($productType->exists)
                    <h2>Update Product Types</h2>
                @else
                    <h2>Add Product Types</h2>
                @endif
                <form action="{{$productType->exists ? route('product-types.update', ['product_type'=>$productType->id]): route('product-types.store')}}"
                      method="POST" role="form">
                    {{csrf_field()}}
                    @if($productType->exists)
                        {{method_field('PUT')}}
                    @endif

                    <div class="form-group">
                        <label for="type-name">Name*:</label>
                        <input type="text" class="form-control" id="type-name"
                               placeholder="Enter Name" name="name"
                               value="{{$productType->exists? $productType->name:''}}" required>
                    </div>
                    <div id="form-wrapper" class="attribute-set mb-2">
                        <h4>Attributes*:</h4>
                        @if($productType->exists)
                            @foreach($productType->attributes as $attributes)
                                <div class="form-row">
                                    @foreach($attributes as $key=>$value)
                                        <div class="col">
                                            <input type="text" class="form-control"
                                                   name="attributes[{{$loop->parent->index}}][{{$key}}]"
                                                   value="{{$value}}" placeholder="Attribute Key">
                                        </div>
                                    @endforeach
                                    <i class="far fa-minus-square fa-lg text-danger remove-icon"></i>
                                </div>

                            @endforeach
                        @else
                            @for($i=0; $i < 2; $i++)
                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" class="form-control" name="attributes[{{$i}}][key]"
                                               value="" placeholder="Attribute Key">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" name="attributes[{{$i}}][type]"
                                               placeholder="Attribute Type">
                                    </div>
                                </div>
                            @endfor
                        @endif
                    </div>
                    <i id="add-new-attribute" class="far fa-plus-square fa-lg text-success cursor-pointer"></i>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection