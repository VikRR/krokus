@extends('layouts.app')

@section('content')
    <div class="product-types">
        <div class="row justify-content-center">
            <div class="col-md-8 offset-2">
                <div class="d-flex justify-content-between align-items-center">
                    <h2>Product Types List</h2>
                    <a href="{{route('product-types.create')}}">
                        <i class="far fa-plus-square fa-lg text-success"></i>
                    </a>
                </div>
                @empty(!count($productTypes))
                    @foreach($productTypes as $type)
                        <div class="card mb-1">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <h4>{{$type->name}}</h4>
                                    <a href="{{route('product-types.edit', ['product_type'=>$type->id])}}">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </div>
                                <h5>Attributes</h5>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Type</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($type->attributes as $attributes)
                                        <tr>
                                            @foreach($attributes as $attr)
                                                <td>{{$attr}}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    @endforeach
                @else
                    <h3>Product Types Not Found</h3>
                @endempty

            </div>
        </div>
    </div>
@endsection