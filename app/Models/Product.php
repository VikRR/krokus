<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['name', 'attributes'];

    protected $casts = [
        'attributes'=> 'array'
    ];

    public function type()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function scopeOfType($query, $typeId)
    {
        return $query->whereHas('type', function ($q) use ($typeId) {
            $q->where('id', $typeId);
        });
    }
}
