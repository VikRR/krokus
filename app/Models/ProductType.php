<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = ['name', 'attributes'];

    protected $casts = [
        'attributes'=> 'array'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
