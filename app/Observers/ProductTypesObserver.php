<?php

namespace App\Observers;

use App\Models\ProductType;
use App\Services\ProductService;
use Illuminate\Support\Arr;

class ProductTypesObserver
{
    public $productService = null;

    public function __construct(ProductService $service)
    {
        $this->productService = $service;
    }

    /**
     * Handle the product type "updated" event.
     *
     * @param  ProductType  $productType
     *
     * @return void
     */
    public function updated(ProductType $productType)
    {
        $changes = $productType->getChanges();
        if (Arr::has($changes, 'attributes')) {
            $this->productService->updateProductAttributesByProductType($productType);
        }
    }
}
