<?php

namespace App\Providers;

use App\Models\ProductType;
use App\Observers\ProductTypesObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ProductType::observe(ProductTypesObserver::class);
    }
}
