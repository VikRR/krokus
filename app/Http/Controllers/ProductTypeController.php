<?php

namespace App\Http\Controllers;

use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $productTypes = Cache::remember('productTypes', Carbon::now()->addMinutes(30), function(){
            return ProductType::all();
        });

        return view('product-types.index', compact('productTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('product-types.create', ['productType' => new ProductType()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        ProductType::create($request->only(['name', 'attributes']));
        Cache::forget('productTypes');

        return redirect()->route('product-types.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $productType = ProductType::findOrFail($id);

        return view('product-types.create', compact('productType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $productType = ProductType::findOrFail($id);
        $productType->update($request->only(['name', 'attributes']));
        Cache::forget('productTypes');

        return redirect()->route('product-types.index');
    }

}
