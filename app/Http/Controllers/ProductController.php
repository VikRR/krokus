<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Cache::remember('products', Carbon::now()->addMinute(30), function () {
            return Product::with('type')->get();
        });

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $productTypes = Cache::remember('productTypes', Carbon::now()->addMinute(30), function () {
            return ProductType::all(['id', 'name', 'attributes']);
        });
        $product = new Product();

        return view('products.create', compact(['productTypes', 'product']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $attr    = $request->all();
        $type    = ProductType::find($attr['type']);
        $product = new Product;
        $product->fill([
            'name'       => $attr['name'],
            'attributes' => $attr['attributes'],
        ]);
        $type->products()->save($product);
        Cache::forget('products');

        return response()->json(['data' => $product]);
    }
}
