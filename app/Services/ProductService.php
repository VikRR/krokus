<?php


namespace App\Services;


use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Support\Arr;

class ProductService
{

    public function updateProductAttributesByProductType(ProductType $productType)
    {
        $productTypeAttributes = [];

        foreach (Arr::pluck($productType->attributes, 'key') as $attribute) {
            $productTypeAttributes[$attribute] = '';
        }

        $products = Product::ofType($productType->id)->get();

        foreach ($products as $product) {
            $this->updateAttribute($product, $productTypeAttributes);
        }
    }

    protected function updateAttribute(Product $product, array $productTypeAttributes)
    {
        $product->update([
            'attributes' => [
                array_replace($productTypeAttributes,
                    array_intersect_key($product->attributes[0], $productTypeAttributes))
            ]
        ]);
    }
}