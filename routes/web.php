<?php

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products', 'ProductController')->only(['index','edit', 'create', 'store']);
Route::resource('product-types', 'ProductTypeController')->except(['show', 'destroy']);
