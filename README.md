## Task

Create a dynamically linked model of data. Suppose we  have a model Product(eg. sku123, sku124 ...). Each individual entity is mapped to the ProductType(phone, pad, laptop ...) which has a set of attributes (eg. weight: string, height: integer, price: float...), which are in turn available to the corresponding Product model, as well as can be used for searching and filtering. Also, you must provide for the ability to change the set of attributes with the subsequent update of all affected Products (db garbage collection). Also it would be good to provide caching functionality in order to maintain high load optimization.
Provide a basic user input forms to control and populate new Product and ProductType records, no security concerns are required(no authorization).
All database manipulations must be performed through migrations. Seeds may be used for testing and dummy data population.
