<?php

use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $laptopAttr = json_encode([
            ['key' => 'display', 'type' => 'float'],
            ['key' => 'size', 'type' => 'number'],
        ]);
        \DB::table('product_types')->insert([
            'name'       => 'Laptop',
            'attributes' => $laptopAttr,
        ]);
        $pcAttr = json_encode([
            ['key' => 'weight', 'type' => 'string'],
            ['key' => 'size', 'type' => 'number'],
        ]);
        \DB::table('product_types')->insert([
            'name'       => 'PC',
            'attributes' => $pcAttr,
        ]);
    }
}
