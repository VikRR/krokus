<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $laptopAttr = json_encode([
            ['display' => '17.5', 'size' => '22x33sm']
        ]);
        $pcAttr = json_encode([
            ['weight' => '2kg', 'size' => '22.4']
        ]);
        for ($i = 1; $i <= 3; $i++) {
            \DB::table('products')->insert([
                'name'            => "Laptop-$i",
                'product_type_id' => 1,
                'attributes'      => $laptopAttr,
            ]);
            \DB::table('products')->insert([
                'name'            => "PC-$i",
                'product_type_id' => 2,
                'attributes'      => $pcAttr,
            ]);
        }
    }
}
